from ics import Calendar
import urllib.request
import csv

# Open the CSV file and read the values to the variables
with open('input.csv', 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        keyword, output_file, url, new_url = row

        # Download and parse the ICS file
        ics_file_data = urllib.request.urlopen(url).read().decode()
        
        # Save the original file
        with open(output_file+'_original.ics', 'w') as f:
            f.write(ics_file_data)

        # Parse the ICS data
        c = Calendar(ics_file_data)

        # Loop through each event in the calendar
        for event in c.events:
            # Check whether the current event URL contains the old keyword
            if keyword in str(event.url):
                event.url = new_url   # If it does, replace whole URL with new URL
            if keyword in str(event.description):
                event.description = new_url
        
        # Write the modified calendar back to the new file
        with open(output_file+'_updated.ics', 'w') as g:
            g.writelines(c)
